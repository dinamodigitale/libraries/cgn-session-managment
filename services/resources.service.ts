import { Injectable } from '@angular/core';
import { Restify } from './api-service/restify.service';
import { ApiService } from './api-service/api.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ResourcesService extends Restify<any> {
  basePath = '/resources';

  constructor(protected apiService: ApiService) {
    super(apiService);
  }

  getSpecific(data): Observable<any> {
    return this.apiService.post(`${this.basePath}/specific`, data)
  }
  
}
