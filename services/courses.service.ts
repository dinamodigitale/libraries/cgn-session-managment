import { Injectable } from '@angular/core'
import { ApiService } from './api-service/api.service'
import { Restify } from './api-service/restify.service'

@Injectable({
  providedIn: 'root',
})
export class CoursesService extends Restify<any> {
  basePath = '/backend/courses'

  constructor(protected apiService: ApiService) {
    super(apiService)
  }

  apps(provider: string[]) {
    return this.apiService.post(this.basePath + '/apps', {
      provider,
    })
  }

  coursesByCompanyProvider(providerId: string) {
    return this.resource({
      query: {
        provider: providerId,
      },
    })
  }
}
