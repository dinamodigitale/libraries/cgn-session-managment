import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ApiService } from "./api-service/api.service";
import { Restify } from "./api-service/restify.service";

@Injectable({
  providedIn: "root",
})
export class SessionProgressService extends Restify<any> {
  basePath = "/backend/session-progress";
  
  constructor(protected apiService: ApiService) {
    super(apiService);
  }

  
}
