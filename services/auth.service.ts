import { tap, map } from "rxjs/operators";
import { Injectable } from "@angular/core";
import { HttpResponse, HttpHeaders } from "@angular/common/http";
import { Observable, BehaviorSubject, Subject } from "rxjs";
import { ApiService } from "./api-service/api.service";
import { ROLES } from "@app/app/interfaces/roles";

export interface User {
  id: string;
  _id: string;
  name: string;
  surname: string;
  email: string;
  role: ROLES;
  business: string;
  provider: any;
  company: any;
  pin: string;
  [x: string]: any;
}

@Injectable({
  providedIn: "root",
})
export class AuthService {
  private loggedUser = new BehaviorSubject<User | undefined>(undefined);

  constructor(private apiService: ApiService) {}

  login(email: string, password: string): Observable<HttpResponse<User>> {
    return this.apiService
      .post(
        "/auth",
        {
          email,
          password,
        },
        {
          observe: "response",
        }
      )
      .pipe(tap((res: any) => this.saveUserInfo(res)));
  }

  private saveUserInfo(user): void {
    localStorage.setItem("userInfoFE", JSON.stringify(user));
    this.loggedUser.next(user);
  }

  removeUserInfo(): void {
    localStorage.removeItem("userInfoFE");
    localStorage.removeItem("tokenFE");
    localStorage.removeItem("uidFE");
    this.loggedUser.next(null);
  }

  getUserInfo(): User {
    return JSON.parse(localStorage.getItem("userInfoFE"));
  }

  get getLoggedUser() {
    return this.loggedUser.asObservable();
  }

  logout() {
    // force header in logout
    const headers = new HttpHeaders({
      Authorization: `Bearer ${localStorage.getItem("tokenFE")}`,
      "X-UID": `${localStorage.getItem("uidFE")}`,
    });
    return this.apiService.delete(`/auth/logout`, { headers: headers }).pipe(
      tap((user) => {
        this.removeUserInfo();
      })
    );
  }

  public isLoggedIn() {
    return this.apiService
      .post("/auth/verify", {
        token: localStorage.getItem("tokenFE"),
        uid: localStorage.getItem("uidFE"),
      })
      .pipe(
        tap((res: any) => this.saveUserInfo(res)),
        map((user) => {
          return !!user["_id"];
        })
      );
  }

  generateToken(email: string) {
    return this.apiService.post("/auth/reset-password-request", {
      email,
    });
  }

  validatePasswordReset(
    email: string,
    token: string,
    password: string,
    passwordConfirmation: string
  ) {
    return this.apiService.post("/auth/reset-password", {
      email,
      token,
      password,
      passwordConfirmation,
    });
  }
}
