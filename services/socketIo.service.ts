import { Injectable } from "@angular/core";
import { map } from "rxjs/operators";
import { io } from "socket.io-client";

@Injectable()
export class SocketService {
  socket = io('http://localhost:3000')
}