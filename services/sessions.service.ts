import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { SessionInterface, SessionProgressInterface } from "../interfaces/session";
import { ApiService } from "./api-service/api.service";
import { Restify } from "./api-service/restify.service";

export interface ProgressInterface {
  appId: any;
  chapter: string;
  user: string;
  restart: boolean;
  createdBy: string;
  timestamps: Date;
  progress?: number;
  payload?: object;
  message?: string;
}
@Injectable({
  providedIn: "root",
})
export class SessionsService extends Restify<any> {
  basePath = "/backend/sessions";

  constructor(protected apiService: ApiService) {
    super(apiService);
  }

  getArchivedSessions(): Observable<SessionInterface[]> {
    return this.apiService.get(`${this.basePath}/archived`);
  }

  resetAllSessionProgress(sessionId: string): Observable<void> {
    return this.apiService.get(`${this.basePath}/${sessionId}/reset/all`);
  }

  getAllUserProgress(
    sessionId: string
  ): Observable<{
    [x: string]: SessionProgressInterface[];
  }> {
    return this.apiService.get(`${this.basePath}/${sessionId}/all-progress`);
  }
  getUserProgress(
    sessionId: string,
    userId: string
  ): Observable<SessionProgressInterface[]> {
    return this.apiService.get(
      `${this.basePath}/${sessionId}/user-progress/${userId}`
    );
  }

  getLastUsersProgress(
    sessionId: string
  ): Observable<{
    [x: string]: SessionProgressInterface | null;
  }> {
    return this.apiService.get(
      `${this.basePath}/${sessionId}/last-progress/all`
    );
  }

  getAppsByAppId(appsId: any) {
    return this.apiService.post(this.basePath + "/appId", {
      appsId,
    });
  }

  createSessionFromCourseIndex(course: any, newSessionOptions: any) {
    return this.apiService.post(this.basePath + "/createSessionFromCourse", {
      course,
      newSessionOptions,
    });
  }

  stream(sessionId: string): Observable<any> {
    return this.apiService.get(
      this.basePath + "/stream?session=" + sessionId,
      {}
    );
  }

  apps(provider: string[]) {
    return this.apiService.post(this.basePath + "/apps", {
      provider,
    });
  }

  progress(
    prog: Partial<SessionProgressInterface>,
    sessionId: string,
    users: string[] = null,
    mapped = null
  ) {
    return this.apiService.post(`${this.basePath}/${sessionId}/progress`, {
      prog,
      users,
      mapped,
    });
  }

  getUserLastProgress(sessionId: string): Observable<ProgressInterface> {
    return this.apiService.get(`${this.basePath}/${sessionId}/last-progress`);
  }

  restartUserChapter(
    prog: Partial<ProgressInterface>,
    sessionId: string,
    userId: string
  ) {
    return this.apiService.post(
      `${this.basePath}/${sessionId}/restart/${userId}`,
      prog
    );
  }

  resetAllProgress(sessionId: string) {
    return this.apiService.get(`${this.basePath}/${sessionId}/reset/all`);
  }

  resetUsersProgress(sessionId: string, users: string[]) {
    return this.apiService.post(`${this.basePath}/${sessionId}/reset/multiple`, {users});
  }

  getSessionByCourseId(courseId: string) {
    return this.apiService.post(this.basePath + "/course", { courseId });
  }
}
