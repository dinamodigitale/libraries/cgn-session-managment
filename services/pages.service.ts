import { Injectable } from '@angular/core';
import { Restify } from './api-service/restify.service';
import { ApiService } from './api-service/api.service';
import { Observable } from 'rxjs';

type ContentType = 'menu' | 'magazine' | 'profile' | '/pages/path' | 'stream' | 'register'
@Injectable({
  providedIn: "root",
})
export class PagesService extends Restify<any> {
  basePath = "/frontend/pages";

  homepageBasePath = "front-page";

  constructor(protected apiService: ApiService) {
    super(apiService);
  }

  getContent(content: ContentType) {
    return super.get(content);
  }

  getMenuVoices(): Observable<any> {
    return super.get("menu");
  }

  getPage(slug): Observable<any> {
    return super.get(slug);
  }

  getHomepage(): Observable<any> {
    return super.get(this.homepageBasePath);
  }
}
