import { Injectable, Renderer2, RendererFactory2 } from "@angular/core";
import { BehaviorSubject } from "rxjs";

export type HeaderMode = "" | "menu" | "login" | "user";

const modeClasses = {
  "": "",
  menu: "menu-open",
  login: "login-open",
  user: "user-open",
};

@Injectable({
  providedIn: "root",
})
export class HeaderService {
  private headerMode = new BehaviorSubject<HeaderMode>("");
  private renderer: Renderer2;

  constructor(private rendererFactory: RendererFactory2) {
    this.renderer = rendererFactory.createRenderer(null, null);
  }

  get currentMode() {
    return this.headerMode.asObservable();
  }

  setMode(mode: HeaderMode) {
    if (
      this.headerMode.getValue() &&
      document
        .getElementsByTagName("body")[0]
        .classList.value.split(" ")
        .includes(modeClasses[this.headerMode.getValue()])
    ) {
      this.renderer.removeClass(
        document.body,
        modeClasses[this.headerMode.getValue()]
      );
      this.renderer.removeClass(
        document.getElementsByTagName("html")[0],
        "scroll-block"
      );
    }
    if (mode) {
      this.renderer.addClass(document.body, modeClasses[mode]);
      this.renderer.addClass(
        document.getElementsByTagName("html")[0],
        "scroll-block"
      );
    }
    this.headerMode.next(mode);
  }
}
