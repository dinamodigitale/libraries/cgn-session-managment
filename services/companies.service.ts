import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { CompanyInterface } from '../interfaces/users/company'
import { ProviderInterface } from '../interfaces/users/provider'
import { ApiService } from './api-service/api.service'
import { Restify } from './api-service/restify.service'

@Injectable({
  providedIn: "root",
})
export class CompaniesService extends Restify<any> {
  basePath = "/backend/companies";

  constructor(protected apiService: ApiService) {
    super(apiService);
  }

  getAllCompanyUsers(companyId: CompanyInterface["_id"]): Observable<any> {
    return this.apiService.get(this.basePath + "/users/" + companyId);
  }

  getCompanyByProvider(providerId: ProviderInterface["_id"]): Observable<any> {
    return this.apiService.get(this.basePath + "/company-by-provider/" + providerId);
  }
}
