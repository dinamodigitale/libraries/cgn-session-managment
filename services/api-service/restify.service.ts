import { ApiService } from './api.service'
import { Observable } from 'rxjs'

export abstract class Restify<T> {
  abstract basePath: string;

  constructor(protected apiService: ApiService) {}

  index(queryString: any = ""): Observable<any> {
    return this.apiService.get(this.basePath + queryString);
  }

  get(id): Observable<T> {
    return this.apiService.get(`${this.basePath}/${id}`);
  }

  resource(data, params = {}): Observable<T> {
    return this.apiService.post(`${this.basePath}/resource`, data, params);
  }

  create(data, params = {}): Observable<T> {
    return this.apiService.post(this.basePath, data, params);
  }

  update(id, data, params = {}): Observable<any> {
    return this.apiService.patch(`${this.basePath}/${id}`, data, params);
  }
}
