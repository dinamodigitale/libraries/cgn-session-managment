import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { ApiService } from './api-service/api.service'
import { Restify } from './api-service/restify.service'

@Injectable({
  providedIn: 'root',
})
export class UsersService extends Restify<any> {
  basePath = '/backend/users'

  constructor(protected apiService: ApiService) {
    super(apiService)
  }

  searchByMail(keyword: string, limit: number = 5, body = {}): Observable<any> {
    return this.resource(
      {
        query: {
          $or: [
            { name: { $regex: keyword, $options: 'i' } },
            { surname: { $regex: keyword, $options: 'i' } },
            { email: { $regex: keyword, $options: 'i' } },
          ],
        },
        ...body,
      },
      { params: { limit: limit } }
    )
  }

  belongingUsers(): Observable<any> {
    return this.resource({
      query: {
        role: ['provider', 'company'],
      },
    })
  }

  companies(): Observable<any> {
    return this.resource({
      query: {
        role: ['company'],
      },
    })
  }

  usersByCompany(companyId): Observable<any> {
    return this.resource({
      query: {
        role: ['user'],
        company: companyId,
      },
    })
  }
}
