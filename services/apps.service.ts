import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { ApiService } from './api-service/api.service'
import { Restify } from './api-service/restify.service'

@Injectable({
  providedIn: 'root',
})
export class AppsService extends Restify<any> {
  basePath = '/backend/catalogue/apps'

  constructor(protected apiService: ApiService) {
    super(apiService)
  }

  appsByCategory(category: string): Observable<any> {
    return this.apiService.get(this.basePath + '?category=' + category, {})
  }

  getSpecificItems(ids: string[]): Observable<any> {
    return this.apiService.post(`${this.basePath}/specific`, ids)
  }
}
