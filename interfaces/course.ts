import { User } from "../services/auth.service";
import { AppChapterInterface, AppInterface } from "./app";
import { AppCategoryInterface } from "./app-category";
import { CompanyInterface } from "./users/company";
import { ProviderInterface } from "./users/provider";

export interface CourseInterface {
  _id: string;
  id: String
  title: String;
  description: String;
  slug: String;
  cover: any;
  private: Boolean;
  visibleOnFe: Boolean;
  appCategory: AppCategoryInterface;
  apps: Array<CourseAppChapterInterface>;
  tags: Array<String>;
  partials: Array<any>;
  companies: Array<CompanyInterface>;
  provider: ProviderInterface;
  users: Array<User>;
}

export interface CourseAppChapterInterface {
  appId: AppInterface | String;
  chapterIdentifier:  AppChapterInterface;
}