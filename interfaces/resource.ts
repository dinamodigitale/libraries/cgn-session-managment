export interface ResourceInterface {
  _id: Number,
  [x: string]: any,
}