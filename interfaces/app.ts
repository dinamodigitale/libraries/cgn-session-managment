import { User } from "../services/auth.service";
import { AppCategoryInterface } from "./app-category";
import { AppReleaseInterface } from "./app-release";
import { ResourceInterface } from "./resource";

export interface AppChapterInterface {
  active: any;
  title: string;
  description: string;
  identifier: string;
}


export interface AppInterface {
  id: string
  title: string;
  description: string;
  appId: string;
  slug: string;
  cover: ResourceInterface;
  category: AppCategoryInterface;
  tags: Array<string>;
  private: Boolean;
  visibleOnFe: Boolean;
  partials: Array<any>;
  users: Array<User>;
  releases: Array<AppReleaseInterface>;
  docs: ResourceInterface;
  chapters: Array<AppChapterInterface>;
}
