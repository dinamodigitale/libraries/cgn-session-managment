import { ResourceInterface } from "./resource";

export interface AppCategoryInterface {
  _id: string;
  title: String;
  description: String;
  slug: String;
  cover: ResourceInterface;
}