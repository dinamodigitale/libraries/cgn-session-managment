import { ResourceInterface } from "./resource";

export interface AppReleaseInterface {
  description: String;
  slug: String;
  cover: ResourceInterface;
  appFile: ResourceInterface;
  // channel: AppReleaseChannel;
  chapter: string
}
