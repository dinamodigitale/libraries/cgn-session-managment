type LogTtype = "error" | "success" | "pending";

export interface LogInterface {
  autoHide?: boolean;
  open: boolean;
  type: LogTtype;
  message: string;
  status: string | number;
  statusText: string;
  ok: boolean;
  name: string;
  error: {
    err: string;
  };
}
