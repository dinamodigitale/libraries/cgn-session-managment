import { DeviceInterface } from "../device";
import { ProviderInterface } from "./provider";

export interface CompanyInterface {
  _id; string;
  name: String;
  thumbnail: String;
  devices: Array<DeviceInterface>;
  provider: ProviderInterface | string
  partials: Array<any>;
}
