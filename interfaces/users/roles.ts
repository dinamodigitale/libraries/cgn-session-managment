enum ROLES {
  ADMIN = 'admin',
  PROVIDER = 'provider',
  COMPANY = 'company',
  USER = 'user',
}

type RoleType = 'admin' | 'provider' | 'company' | 'user';

const FormattedRoles = [
  {
    label: 'Amministratore',
    value: 'admin',
  },
  {
    label: 'Provider',
    value: 'provider',
  },
  {
    label: 'Azienda',
    value: 'company',
  },
  {
    label: 'Utente',
    value: 'user',
  },
];

export { ROLES, RoleType, FormattedRoles };
