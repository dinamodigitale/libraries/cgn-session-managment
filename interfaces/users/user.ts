import { RoleType } from "@app/app/interfaces/roles";
import { CompanyInterface } from "./company";
import { ProviderInterface } from "./provider";

export interface OLDUserInterface {
  biography: String;
  createdAt: string;
  email: string;
  id: string;
  name: string;
  referente: string;
  roles: { name: string }[];
  surname: string;
  thumbnail: string;
}
