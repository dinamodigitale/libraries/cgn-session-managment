import { ResourceInterface } from "../resource";
import { ProviderCompanyInterface } from "./common";

export interface ProviderInterface extends ProviderCompanyInterface {
  _id: string;
  name: String;
  domain: String;
  contacts: String;
  thumbnail: ResourceInterface;
  partials: Array<any>;
}
