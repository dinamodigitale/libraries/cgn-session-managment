export interface ProviderCompanyInterface {
  name: String;
  thumbnail: Object;
  _id: string;
}
