import { User } from "../services/auth.service";
import { AppChapterInterface, AppInterface } from "./app";
import { CourseInterface } from "./course";
import { CompanyInterface } from "./users/company";

type SessionsStatusType = "created" | "started" | "archived";


export interface SessionDefaultChapter {
  appId: string;
  chapter: string;
}

export interface SessionInterface {
  defaultApp: string;
  _id: string;
  id: String;
  title: String;
  description: String;
  slug: String;
  course: CourseInterface;
  users: Array<User>;
  company: CompanyInterface;
  status: SessionsStatusType;
  notes: Array<String>;
  // progress: Array<any>;
  createdBy: User | String;
  // defaultChapters: Array<SessionDefaultChapter>;
  appId: AppInterface['appId'];
  chapter: string;
}

export interface SessionProgressInterface {
  appId: AppInterface['appId'];
  chapter: string;
  user: Pick<User, "_id"> | string;
  session: Pick<SessionInterface, "_id">;
  payload: Object;
  message: String;
  restart: Boolean;
  progress: Number;
  createdBy: Pick<User, "_id">;
  timestamp: Date;
}
