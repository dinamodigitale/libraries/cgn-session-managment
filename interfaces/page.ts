import { User } from "../services/auth.service";

export interface PageInterface {
  title: string;
  description: string;
  partials: Array<any>;
  slug: string;
  cover: object;
  seo: object;
  published: boolean;
  showInMenu: boolean;
  users: Array<User>;
}
