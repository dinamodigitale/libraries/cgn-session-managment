export interface DeviceInterface {
  name: String;
  deviceId: String;
  registerDate: Date;
  company: String;
  enabled?: Boolean;
}
