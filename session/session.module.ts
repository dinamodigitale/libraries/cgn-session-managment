import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SessionComponent } from './session.component';
import { UIRouterModule } from '@uirouter/angular';
import { SessionManagmentModule } from './session-managment/session-managment.module';
import { SessionIndexModule } from './session-index/session-index.module';
import { SocketService } from '../services/socketIo.service';


// const socketConfig: SocketIoConfig = {
//   url: "http://localhost:3000",
//   options: {},
// };

@NgModule({
  declarations: [SessionComponent],
  imports: [
    CommonModule,
    UIRouterModule.forChild({
      states: [
        {
          abstract: true,
          name: "app.private.session",
          url: "session",
          component: SessionComponent,
        },
      ],
    }),
    // SessionManagmentModule,
    SessionIndexModule,
  ],
  providers: [SocketService]
})
export class SessionModule {}
