import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SessionManagmentComponent } from './session-managment.component';
import { UIRouterModule } from '@uirouter/angular';
import { CreateSessionFormModule } from '@session-managment/components/create-session-form/create-session-form.module';
import { QuickCreateSessionFormModule } from '@session-managment/components/quick-create-session-form/quick-create-session-form.module';



@NgModule({
  declarations: [SessionManagmentComponent],
  imports: [
    CommonModule,
    UIRouterModule.forChild({
      states: [
        {
          name: 'app.private.session-manag',
          url: '/create-session?course',
          component: SessionManagmentComponent
        }
      ]
    }),
    CreateSessionFormModule,
    QuickCreateSessionFormModule
  ],
  exports: [SessionManagmentComponent]
})
export class SessionManagmentModule { }
