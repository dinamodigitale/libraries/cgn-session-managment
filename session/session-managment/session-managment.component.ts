import { Component, OnInit } from "@angular/core";
import { AuthService, User } from "@session-managment/services/auth.service";
import { Transition } from "@uirouter/angular";

@Component({
  selector: "app-session-managment",
  templateUrl: "./session-managment.component.html",
  styleUrls: ["./session-managment.component.scss"],
})
export class SessionManagmentComponent implements OnInit {
  user: Partial<User> = null;

  injectedOptions = {
    provider: null,
    // company: null,
    course: null,
  };

  isQuickForm = false;

  constructor(public transition: Transition, private authService: AuthService) {
    if (this.transition.params().course) {
      this.isQuickForm = true;
    }
  }

  ngOnInit(): void {
    if (this.isQuickForm) {
      this.user = this.authService.getUserInfo();
      if (
        this.transition.params().course &&
        this.user &&
        this.user.provider &&
        this.user.provider._id
      ) {
        this.injectedOptions.provider = this.user.provider._id;
        this.injectedOptions.course = this.transition.params().course;
      } else {
        console.log("User non contiene info necessarie per quickForm", this.user)
      }
    }
  }
}
