import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-session',
  template: '<ui-view></ui-view>',
  // styleUrls: ['app-styles.scss']
})
export class SessionComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
