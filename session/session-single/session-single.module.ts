import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SessionSingleComponent } from "./session-single.component";
import { StateService, Transition, UIRouterModule } from "@uirouter/angular";
import { forkJoin } from "rxjs";
import { SessionsService } from "@session-managment/services/sessions.service";
import { SessionHeaderModule } from "@session-managment/components/session-header/session-header.module";
import { SessionIndexProgressModule } from "@session-managment/components/session-index-progress/session-index-progress.module";
import { AccordionModule } from "@session-managment/components/accordion/accordion.module";
import { GroupSelectModule } from "@session-managment/forms/group-select/group-select.module";
import { ProSelectModule } from "@session-managment/forms/pro-select/pro-select.module";
import { SessionProgressFiltersModule } from "@session-managment/components/session-progress-filters/session-progress-filters.module";
import { SessionSingleUserProgressModule } from "@session-managment/components/session-single-user-progress/session-single-user-progress.module";
import { SessionBottomBarModule } from "@session-managment/components/session-bottom-bar/session-bottom-bar.module";

export async function resolveSession(
  sessionService: SessionsService,
  transition: Transition,
  stateService: StateService
) {
  const slug = transition.params().slug;
  return sessionService
    .get(slug)
    .toPromise()
    .catch((e) => {
      console.log("error while retrieving session");
    });
}

@NgModule({
  declarations: [SessionSingleComponent],
  imports: [
    CommonModule,
    UIRouterModule.forChild({
      states: [
        {
          name: "app.private.session-single",
          url: "/:slug?detail",
          component: SessionSingleComponent,
          params: {
            detail: {
              dynamic: true,
            },
            t: {
              value: "",
            },
          },
          resolve: [
            {
              provide: "item",
              token: "token",
              useFactory: (
                sessionService: SessionsService,
                transition: Transition
              ) => {
                const slug = transition.params().slug;
                return forkJoin([
                  sessionService.get(slug),
                  sessionService.getAllUserProgress(slug),
                ]).toPromise();
              },
              policy: { when: "LAZY", async: "WAIT" },
              deps: [SessionsService, Transition],
            },
          ],
        },
      ],
    }),
    SessionHeaderModule,
    SessionIndexProgressModule,
    AccordionModule,
    GroupSelectModule,
    ProSelectModule,
    SessionProgressFiltersModule,
    SessionSingleUserProgressModule,
    SessionBottomBarModule,
    AccordionModule,
  ],
})
export class SessionSingleModule {}
