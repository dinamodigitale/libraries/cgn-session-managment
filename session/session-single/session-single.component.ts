import { Component, Inject, OnDestroy, OnInit } from '@angular/core'
import {
  AppChapterInterface,
  AppInterface,
} from '@session-managment/interfaces/app'
import {
  SessionInterface,
  SessionProgressInterface,
} from '@session-managment/interfaces/session'
import { User } from '@session-managment/services/auth.service'
import { SessionsService } from '@session-managment/services/sessions.service'
import { SocketService } from '@session-managment/services/socketIo.service'
import { StateService } from '@uirouter/angular'
import { BehaviorSubject, interval, Observable } from 'rxjs'

@Component({
  selector: 'app-session-single',
  templateUrl: './session-single.component.html',
  styleUrls: ['./session-single.component.scss'],
})
export class SessionSingleComponent implements OnInit, OnDestroy {
  defaultSessionApp: AppInterface = null
  workOnGrid = false
  changingChapter = false

  session: SessionInterface = null
  /**
   * Multiple actions utils
   */
  selectAll$: BehaviorSubject<boolean> = new BehaviorSubject(null)
  selectedUsers: string[] = []

  /**
   * Store all last session progresses {<user._id> : progress, ...}
   */
  sessionProgresses$: BehaviorSubject<{
    [x: string]: SessionProgressInterface | null
  }> = new BehaviorSubject(null)

  /**
   * tutti i progress degli utenti della sessione mappati {<user._id> : SessionProgress[]}
   */
  formattedUsersProgress$: BehaviorSubject<{
    [x: string]: SessionProgressInterface[]
  }> = new BehaviorSubject(null)

  sessionTimer: Observable<any> = interval(1000 * 60)
  subscriptionSessionTimer = null

  constructor(
    @Inject('item') public item: SessionInterface,
    private sessionService: SessionsService,
    // private store: Store,
    public stateService: StateService,
    private socketService: SocketService
  ) {
    this.session = item[0]
    this.sessionService
      .getLastUsersProgress(item[0]._id)
      .subscribe((res: any) => {
        this.sessionProgresses$.next(res)
      })
    this.formattedUsersProgress$.next(item[1])
  }

  ngOnInit(): void {
    // console.log("[session]", this.session);
    this.getAvailableApps()
    this.subscriptionSessionTimer = this.sessionTimer.subscribe((res) => {
      this.refreshPage()
    })
    this.socketService.socket.on(
      `session:progress:${this.session._id}`,
      (msg) => {
        const progresses = this.formattedUsersProgress$.getValue()
        progresses[msg.user] = progresses[msg.user] || []
        progresses[msg.user].unshift(msg)
        this.formattedUsersProgress$.next(progresses)
        const sessionProgresses = this.sessionProgresses$.getValue()
        sessionProgresses[msg.user] = msg
        this.sessionProgresses$.next(sessionProgresses)
        // this.store.dispatch(
        //   newLog({
        //     log: {
        //       autoHide: true,
        //       ok: true,
        //       open: true,
        //       type: "success",
        //       message: `${msg.message}`,
        //       error: {
        //         err: `${msg.message}`,
        //       },
        //     },
        //   })
        // );
      }
    )
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.socketService.socket.off(`session:progress:${this.session._id}`)
    this.subscriptionSessionTimer.unsubscribe()
  }

  setStateParams(user) {
    this.session.users.forEach((u) => {
      if (u.active && user._id !== u._id) {
        u.active = false
      }
    })
    user.active = !user.active
    if (user.active) {
      this.stateService.transitionTo(
        'app.private.session-single',
        {
          detail: user._id,
        },
        {
          location: true,
          inherit: true,
          reload: false,
        }
      )
    } else {
      this.stateService.transitionTo(
        'app.private.session-single',
        {
          detail: null,
        },
        {
          location: true,
          inherit: true,
          reload: false,
        }
      )
    }
  }
  refreshPage() {
    this.stateService
      .go('.', {
        t: Math.random(),
      })
      .then((res) => {
        // this.store.dispatch(
        //   newLog({
        //     log: {
        //       autoHide: true,
        //       ok: true,
        //       open: true,
        //       type: "success",
        //       message: "Pagina aggiornata",
        //       error: {
        //         err: "Pagina aggiornata",
        //       },
        //     },
        //   })
        // );
      })
  }

  getAvailableApps() {
    if (this.session.appId) {
      let query = [this.session.appId]
      this.sessionService.getAppsByAppId(query).subscribe((res) => {
        this.defaultSessionApp = res[0]
      })
    }
  }

  selectAllUsers(select) {
    if (!select) {
      this.selectAll$.next(true)
      const allUsersIds = this.session.users.map((user) => user._id)
      this.selectedUsers = allUsersIds
    } else {
      this.selectAll$.next(false)
      this.selectedUsers = []
    }
  }

  checkBoxClickedAdd(user: User) {
    if (user && user._id) {
      this.selectedUsers.push(user._id)
    }
  }

  checkBoxClickedRemove(user: User) {
    if (user && user._id) {
      const index = this.selectedUsers.findIndex((u) => u === user._id)
      this.selectedUsers.splice(index, 1)
    }
  }

  triggerGridActions() {
    this.workOnGrid = !this.workOnGrid
  }

  selectUserParser(user: any) {
    if (user) {
      return user
    }
    return ''
  }

  childrenParser(company: User) {
    return company.companies
  }

  returnSingleUserChapter(userId) {
    if (
      this.sessionProgresses$.getValue() &&
      this.sessionProgresses$.getValue()[userId]
    ) {
      return this.sessionProgresses$.getValue()[userId]
    }
  }

  manageSessionStatus(status: string) {
    this.sessionService.update(this.session._id, { status }).subscribe(
      (res) => {
        Object.assign(this.session, res)
        // this.store.dispatch(
        //   newLog({
        //     log: {
        //       autoHide: true,
        //       ok: true,
        //       open: true,
        //       type: "success",
        //       message: "Session aggiornata con successo",
        //       error: {
        //         err: "Session aggiornata con successo",
        //       },
        //     },
        //   })
        // );
        this.stateService.go('.', null, { reload: true })
      },
      (err) => {
        // this.store.dispatch(
        //   newLog({
        //     log: {
        //       autoHide: false,
        //       ok: false,
        //       open: true,
        //       type: "error",
        //       message: "Session aggiornata con successo",
        //       error: {
        //         err: `${err.error.message || "Errore durante l'operazione"}`,
        //       },
        //     },
        //   })
        // );
      }
    )
  }

  manageSessionUsers(event, removing = false) {
    let currentUsers = this.session.users
    if (!removing) {
      Object.assign(currentUsers, event)
    } else if (this.session.users && this.session.users.length) {
      currentUsers = currentUsers.filter(
        (user) => !this.selectedUsers.includes(user._id)
      )
      this.session.users = currentUsers
    }

    this.sessionService
      .update(this.session._id, {
        users: currentUsers,
        company: this.session.company,
      })
      .subscribe(
        (res) => {
          // this.store.dispatch(
          //   newLog({
          //     log: {
          //       autoHide: true,
          //       ok: true,
          //       open: true,
          //       type: "success",
          //       message: "Utente rimosso con successo",
          //       error: {
          //         err: `Utente ${
          //           removing ? "rimosso" : "aggiunto"
          //         } con successo`,
          //       },
          //     },
          //   })
          // );
        },
        (err) => {
          // this.store.dispatch(
          //   newLog({
          //     log: {
          //       autoHide: false,
          //       ok: false,
          //       open: true,
          //       type: "error",
          //       message: "Errore azioni user",
          //       error: {
          //         err: `${err.error.message || "Errore durante l'operazione"}`,
          //       },
          //     },
          //   })
          // );
        }
      )
  }

  resetAllSessionProgress() {
    this.sessionService.resetAllProgress(this.session._id).subscribe((res) => {
      // console.log("res", res.message);
      this.sessionProgresses$.next({})
      this.formattedUsersProgress$.next({})
      // this.store.dispatch(
      //   newLog({
      //     log: {
      //       autoHide: true,
      //       ok: true,
      //       open: true,
      //       type: "success",
      //       message: "Sessione resettata",
      //       error: {
      //         err: "Session resettata con successo",
      //       },
      //     },
      //   })
      // );
    })
  }

  resetSelectedUsersProgress() {
    if (!this.selectedUsers || !this.selectedUsers.length) {
      // this.store.dispatch(
      //   newLog({
      //     log: {
      //       autoHide: false,
      //       ok: false,
      //       open: true,
      //       type: "error",
      //       message: "Devi selezionare almeno un utente",
      //       error: {
      //         err: "Devi selezionare almeno un utente",
      //       },
      //     },
      //   })
      // );
      return
    }
    this.sessionService
      .resetUsersProgress(this.session._id, this.selectedUsers)
      .subscribe((res) => {
        // this.store.dispatch(
        //   newLog({
        //     log: {
        //       autoHide: true,
        //       ok: true,
        //       open: true,
        //       type: "success",
        //       message: "Progress resettati con successo",
        //       error: {
        //         err: "Progress resettati con successo",
        //       },
        //     },
        //   })
        // );
        // aggiorna OBSERVABLE
      })
  }

  setNewActiveChapter(chapter: AppChapterInterface, user: User) {
    console.log(chapter)
    this.sessionService
      .progress(
        {
          appId: this.defaultSessionApp.appId,
          chapter: chapter.identifier,
          user: user._id,
          restart: false,
          payload: {},
          message: `Changed chapter: ${chapter.identifier}`,
          progress: 0,
        },
        this.session._id
      )
      .subscribe((res: SessionProgressInterface) => {
        const currentValue = this.sessionProgresses$.getValue()
        const updatedValue = Object.assign(currentValue, { [user._id]: res })
        this.sessionProgresses$.next(updatedValue)

        const currentValueUserProgresses = this.formattedUsersProgress$.getValue()
        const currentValueUserProgressesArray =
          this.formattedUsersProgress$.getValue()[user._id] || []
        const updatedValueUserProgress = Object.assign(
          currentValueUserProgresses,
          { [user._id]: [res, ...currentValueUserProgressesArray] }
        )
        this.formattedUsersProgress$.next(updatedValueUserProgress)
        // console.log("formattedUsersProgress$", this.formattedUsersProgress$.getValue());
        // this.store.dispatch(
        //   newLog({
        //     log: {
        //       autoHide: true,
        //       ok: true,
        //       open: true,
        //       type: "success",
        //       message: "Capitolo cambiato con successo",
        //       error: {
        //         err: "Capitolo cambiato con successo",
        //       },
        //     },
        //   })
        // );
      })
  }

  restartUserChapter(chapter, user) {
    this.sessionService
      .progress(
        {
          appId: this.defaultSessionApp.appId,
          chapter,
          user: user._id,
          restart: true,
          message: `Restarted chapter ${chapter}`,
        },
        this.session._id
      )
      .subscribe((res) => {
        const currentValue = this.sessionProgresses$.getValue()
        const updatedValue = Object.assign(currentValue, { [user._id]: res })
        this.sessionProgresses$.next(updatedValue)

        const currentValueUserProgresses = this.formattedUsersProgress$.getValue()
        const currentValueUserProgressesArray =
          this.formattedUsersProgress$.getValue()[user._id] || []
        const updatedValueUserProgress = Object.assign(
          currentValueUserProgresses,
          { [user._id]: [res, ...currentValueUserProgressesArray] }
        )
        this.formattedUsersProgress$.next(updatedValueUserProgress)
        // this.store.dispatch(
        //   newLog({
        //     log: {
        //       autoHide: true,
        //       ok: true,
        //       open: true,
        //       type: "success",
        //       message: "Capitolo riavviato con successo",
        //       error: {
        //         err: "Capitolo riavviato con successo",
        //       },
        //     },
        //   })
        // );
      })
  }

  changeMultipleUsersChapters(chapter: AppChapterInterface, restart = false) {
    if (!this.selectedUsers || !this.selectedUsers.length) {
      // this.store.dispatch(
      //   newLog({
      //     log: {
      //       autoHide: false,
      //       ok: false,
      //       open: true,
      //       type: "error",
      //       message: "Devi selezionare almeno un utente",
      //       error: {
      //         err: "Devi selezionare almeno un utente",
      //       },
      //     },
      //   })
      // );
      return
    }
    this.sessionService
      .progress(
        {
          appId: this.defaultSessionApp.appId,
          chapter: chapter.identifier ? chapter.identifier : null,
          restart,
          message: `${restart ? 'Restarted' : 'Changed'} multiple chapter ${
            chapter.identifier ? `: ${chapter.identifier}` : ''
          }`,
        },
        /**
         * session id
         */
        this.session._id,
        /**
         * utenti selezionati
         */
        this.selectedUsers,
        /**
         * se non c'è chapter.identifier sto mandando i capitoli degli utenti mappati.
         * lato api vengono riassegnati
         */
        chapter && chapter.identifier ? null : chapter
      )
      .subscribe((res) => {
        const result = Array.isArray(res) ? res : [res]
        const oldProg = this.sessionProgresses$.getValue()
        result.forEach((prog) => {
          oldProg[prog.user] = prog
        })
        this.sessionProgresses$.next(oldProg)

        const currentValueUserProgresses = this.formattedUsersProgress$.getValue()
        this.selectedUsers.forEach((userId) => {
          currentValueUserProgresses[userId] = currentValueUserProgresses[
            userId
          ]
            ? [
                result.find((prog) => prog.user === userId),
                ...currentValueUserProgresses[userId],
              ]
            : [result.find((prog) => prog.user === userId)]
        })

        this.formattedUsersProgress$.next(currentValueUserProgresses)
      })
  }
}
