import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SessionIndexComponent } from './session-index.component';
import { UIRouterModule } from '@uirouter/angular';
import { AccordionModule } from '@session-managment/components/accordion/accordion.module';
import { SessionIndexItemModule } from '@session-managment/components/session-index-item/session-index-item.module';


@NgModule({
  declarations: [SessionIndexComponent],
  imports: [
    CommonModule,
    UIRouterModule.forChild({
      states: [
        {
          name: "app.private.session-index",
          url: "/sessions",
          component: SessionIndexComponent,
        },
      ],
    }),
    SessionIndexItemModule,
    AccordionModule
  ],
})
export class SessionIndexModule {}
