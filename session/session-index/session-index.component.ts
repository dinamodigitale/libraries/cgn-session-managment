import { Component, OnInit } from '@angular/core';
import { SessionInterface } from '@session-managment/interfaces/session';
import { SessionsService } from '@session-managment/services/sessions.service';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'app-session-index',
  templateUrl: './session-index.component.html',
  styleUrls: ['./session-index.component.scss']
})
export class SessionIndexComponent implements OnInit {
  availableSession: SessionInterface[] = []
  archivedSession: SessionInterface[] = []

  toogleArchived = false

  constructor(private sessionService: SessionsService) { }

  ngOnInit(): void {
    forkJoin([
      this.sessionService.index(),
      this.sessionService.getArchivedSessions(),
    ]).subscribe(res => {
      this.availableSession = res[0].filter((res) => res.status === "started");
      this.archivedSession = res[1]
    })
  }

}
