import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { FeedbackDirective } from './feedback.directive';
import { FeedbackModule } from './feedback/feedback.module';



@NgModule({
  declarations: [FeedbackDirective],
  imports: [
    CommonModule,
    BrowserModule,
    FeedbackModule
  ],
  exports: [FeedbackDirective]
})
export class DirectivesModule { }
