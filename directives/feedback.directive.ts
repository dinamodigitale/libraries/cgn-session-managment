import {
  ApplicationRef,
  ChangeDetectionStrategy,
  Component,
  ComponentFactoryResolver,
  ComponentRef,
  Directive,
  EmbeddedViewRef,
  EventEmitter,
  HostListener,
  Injector,
  Input,
  Output,
  TemplateRef,
  ViewChild,
  ViewContainerRef,
} from "@angular/core";
import { FeedbackComponent } from "./feedback/feedback.component";
// import { AppConfirmDialogComponent } from './confirm/confirm-dialog/confirm-dialog.component';

@Directive({
  selector: "[appFeedback]",
})
export class FeedbackDirective {
  @Input() confirmTitle: string;
  @Input() confirmBody: string;
  @Input() onlyOnChange: boolean;
  @Output() onClicked = new EventEmitter();
  @Output() onChanged = new EventEmitter();
  @Output() emitCancel = new EventEmitter();
  @Output() emitSuccess = new EventEmitter();
  opened = false;

  confirmDialogRef: ComponentRef<FeedbackComponent>;

  constructor(
    private viewContainer: ViewContainerRef,
    private componentFactoryResolver: ComponentFactoryResolver,
    private applicationRef: ApplicationRef,
    private injector: Injector
  ) {}

  @HostListener("keydown", ["$event"])
  public onKeydown(event: Event) {
    console.log("mouse down");
    // event.stopPropagation();
    this.showDialog(event);
  }

  @HostListener("click", ["$event"])
  public onClick(event: Event) {
    if (!this.opened && !this.onlyOnChange) {
      event.stopPropagation();
      this.showDialog(event);
    }
  }
  @HostListener("change", ["$event"])
  public onChange(event: Event) {
    if (!this.opened) {
      event.stopPropagation();
      this.showDialog(event);
    }
  }

  private showDialog(event: Event) {
    this.confirmDialogRef = this.componentFactoryResolver
      .resolveComponentFactory(FeedbackComponent)
      .create(this.injector);

    this.applicationRef.attachView(this.confirmDialogRef.hostView);
    const domElem = (this.confirmDialogRef.hostView as EmbeddedViewRef<any>)
      .rootNodes[0] as HTMLElement;

    document.body.appendChild(domElem);
    this.confirmDialogRef.instance.textInfo.title = this.confirmTitle;
    this.confirmDialogRef.instance.textInfo.body = this.confirmBody;
    this.confirmDialogRef.instance.afterClosed$.subscribe((res) => {
      console.log(res);
      if (res && res.value === "accept" && res.status === "finished") {
        console.log("accepted");
        this.onlyOnChange
          ? this.onChanged.next(event)
          : this.onClicked.next(event);
        this.confirmDialogRef.destroy();
      } else {
        if (res.status === "finished") {
          this.confirmDialogRef.destroy();
        }
        // componentFactory.destroy();
      }
    });
    // const sub = componentFactory.instance.output.subscribe((event) =>
    //   console.log(event)
    // );
    // return componentFactory;
  }
}
