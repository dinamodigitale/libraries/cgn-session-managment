import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  Renderer2,
  RendererFactory2,
  ViewEncapsulation,
} from "@angular/core";
import { BehaviorSubject, Observable, of } from "rxjs";

type StatusType = "pending" | "created" | "finished";
type ValueType = "accept" | "refuse" | "pending";
export interface ResponseFeedbackInterface {
  value: ValueType;
  status: StatusType;
}
@Component({
  selector: "app-feedback",
  templateUrl: "./feedback.component.html",
  styleUrls: ["./feedback.component.scss"],
  encapsulation: ViewEncapsulation.None,
})
export class FeedbackComponent implements OnInit {
  @Input() message: string;
  @Output() close = new EventEmitter();
  @Input() anim = false;

  textInfo = {
    title: null,
    body: null,
  };

  afterClosed$: BehaviorSubject<any> = new BehaviorSubject(false);

  requestSuccess = false;

  private renderer: Renderer2;
  constructor(private rendererFactory: RendererFactory2) {
    this.afterClosed$.next({
      value: "pending",
      status: 'created'
    });
    this.renderer = rendererFactory.createRenderer(null, null);
  }

  ngOnInit() {
    this.renderer.addClass(
      document.getElementsByTagName("html")[0],
      "scroll-block"
    );
    this.anim = true;
    setTimeout(() => {});
  }

  closeCreate(response: boolean) {
    this.anim = false;
    this.afterClosed$.next({
      value: response ? "accept" : 'refuse',
      status: "finished",
    });
    this.renderer.removeClass(
      document.getElementsByTagName("html")[0],
      "scroll-block"
    );
    setTimeout(() => {
      this.close.emit();
    }, 550);
  }
}
