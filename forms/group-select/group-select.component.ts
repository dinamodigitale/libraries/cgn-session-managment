import {
  Component,
  OnInit,
  ContentChild,
  TemplateRef,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  ElementRef,
  HostListener,
} from "@angular/core";
import { GroupSelectActiveDirective } from "./group-select-active.directive";
import { GroupSelectParentDirective } from "./group-select-parent.directive";
import { GroupSelectChildDirective } from "./group-select-child.directive";
import { trigger, transition, style, animate } from "@angular/animations";

@Component({
  selector: "app-group-select",
  templateUrl: "./group-select.component.html",
  styleUrls: ["./group-select.component.scss"],
  animations: [
    trigger("openClose", [
      transition(":enter", [
        style({ opacity: 0, transform: "translateY(-1rem)" }),
        animate(
          ".100s ease-out",
          style({ opacity: 1, transform: "translateY(0rem)" })
        ),
      ]),
      transition(":leave", [
        style({ opacity: 1, transform: "translateY(0rem)" }),
        animate(
          ".100s ease-in",
          style({ opacity: 0, transform: "translateY(-1rem)" })
        ),
      ]),
    ]),
  ],
})
export class GroupSelectComponent implements OnInit {
  open = false;
  @Input() model: any;
  @Output() modelChange: EventEmitter<any> = new EventEmitter();
  @Output() opened: EventEmitter<boolean> = new EventEmitter();
  @Input() options: any[];
  @Input() placeholder = "Select an option";
  @Input() identifier = (item: any) => item._id;
  @Input() search: boolean;
  @Input() searchParser = (item: any, search: string) =>
    item.toLowerCase().includes(search.toLowerCase());
  @ViewChild("searchInput") searchInput: ElementRef<HTMLInputElement>;
  searchValue: string = "";
  @Input() childrenParser = (item) => item.children;
  @Input() disableParent: boolean;

  // TODO: Find a better variable name
  // If this variable is true, by selecting a parent, you will prune all
  // it's own selected children and by selecting a child you will
  // deselect it's own parent
  @Input() inclusive: boolean;

  @ContentChild(GroupSelectActiveDirective, { read: TemplateRef })
  activeTemplate: TemplateRef<any>;
  @ContentChild(GroupSelectParentDirective, { read: TemplateRef })
  parentTemplate: TemplateRef<any>;
  @ContentChild(GroupSelectChildDirective, { read: TemplateRef })
  childTemplate: TemplateRef<any>;

  constructor(private _eref: ElementRef) {}

  ngOnInit(): void {}

  change(value: any, parent: any = null) {
    if (this.model) {
      const found = this.model.findIndex((x) => x === this.identifier(value));
      if (found >= 0) {
        this.model.splice(found, 1);
      } else {
        if (this.inclusive) {
          if (parent) {
            const foundParent = this.model.findIndex(
              (x) => x === this.identifier(parent)
            );
            if (foundParent >= 0) {
              this.model.splice(foundParent, 1);
            }
          } else {
            this.childrenParser(value).forEach((child) => {
              const i = this.model.findIndex((x) => this.identifier(child));
              if (i >= 0) {
                this.model.splice(i, 1);
              }
            });
          }
        }
        this.model.push(this.identifier(value));
        if (!this.inclusive && parent) {
          const p = this.model.findIndex((x) => x === this.identifier(parent));
          if (p < 0) {
            this.model.push(this.identifier(parent));
          }
        }
      }
      this.modelChange.emit(this.model);
    }
  }

  openClose() {
    this.open = !this.open;
    this.opened.emit(this.open);
    if (this.open) {
      this.focusSearch();
    } else {
      this.blurSearch();
    }
  }

  get value() {
    return JSON.stringify(this.model);
  }

  get shownOptions() {
    if (!this.searchValue || !this.search) {
      return this.options;
    }
    return this.options.filter((item) =>
      this.searchParser(item, this.searchValue)
    );
  }

  @HostListener("document:click", ["$event"])
  handleKeyDown(event: MouseEvent) {
    if (this.open && !this._eref.nativeElement.contains(event.target)) {
      this.open = false;
      this.blurSearch();
      this.opened.emit(this.open);
    }
  }

  private focusSearch() {
    setTimeout(() => {
      if (this.searchInput && this.searchInput.nativeElement) {
        this.searchInput.nativeElement.focus();
        this.searchValue = "";
      }
    });
  }
  private blurSearch() {
    setTimeout(() => {
      if (this.searchInput && this.searchInput.nativeElement) {
        this.searchInput.nativeElement.blur();
        this.searchValue = "";
      }
    });
  }

  selectedParent(item: any) {
    if (this.model) {
      return this.model.includes(this.identifier(item));
    }
  }

  selectedChild(child: any, parent: any) {
    return (
      this.model.includes(this.identifier(child)) ||
      (this.model.includes(this.identifier(child)) && this.inclusive)
    );
  }
}
