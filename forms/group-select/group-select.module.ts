import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { GroupSelectComponent } from './group-select.component'
import { GroupSelectChildDirective } from './group-select-child.directive'
import { GroupSelectParentDirective } from './group-select-parent.directive'
import { GroupSelectActiveDirective } from './group-select-active.directive'
import { FormsModule } from '@angular/forms'

@NgModule({
  declarations: [
    GroupSelectComponent,
    GroupSelectChildDirective,
    GroupSelectParentDirective,
    GroupSelectActiveDirective,
  ],
  imports: [CommonModule, FormsModule],
  exports: [
    GroupSelectComponent,
    GroupSelectChildDirective,
    GroupSelectParentDirective,
    GroupSelectActiveDirective,
  ],
})
export class GroupSelectModule {}
