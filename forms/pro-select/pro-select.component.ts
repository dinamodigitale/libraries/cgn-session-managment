import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  ElementRef,
  HostListener,
  ContentChild,
  TemplateRef,
  ViewEncapsulation,
} from "@angular/core";
import { trigger, transition, style, animate } from "@angular/animations";
import { ProSelectOptionDirective } from "./pro-select-option.directive";
import { ProSelectActiveDirective } from "./pro-select-active.directive";

@Component({
  selector: "app-pro-select",
  templateUrl: "./pro-select.component.html",
  styleUrls: ["./pro-select.component.scss"],
  animations: [
    trigger("openClose", [
      transition(":enter", [
        style({ opacity: 0, transform: "translateY(-1rem)" }),
        animate(
          ".100s ease-out",
          style({ opacity: 1, transform: "translateY(0rem)" })
        ),
      ]),
      transition(":leave", [
        style({ opacity: 1, transform: "translateY(0rem)" }),
        animate(
          ".100s ease-in",
          style({ opacity: 0, transform: "translateY(-1rem)" })
        ),
      ]),
    ]),
  ],
  // encapsulation: ViewEncapsulation.None,
})
export class ProSelectComponent implements OnInit {
  @Input() model: any;
  @Output() modelChange: EventEmitter<any> = new EventEmitter();
  @Input() modelParser = (item) => item;
  @Output() optionChange: EventEmitter<any> = new EventEmitter();
  @Output() opened: EventEmitter<boolean> = new EventEmitter();
  @Input() options: any[];
  @Input() placeholder = "Select an option";
  @Input() searchPlaceholder: String = "Search";
  @Input() noResultsMessage: String = "No option available...";

  @Input() enableEmpty = false;
  @Input() noBg;
  @ContentChild(ProSelectOptionDirective, { read: TemplateRef })
  optionTemplate: TemplateRef<any>;
  @ContentChild(ProSelectActiveDirective, { read: TemplateRef })
  activeTemplate: TemplateRef<any>;

  @Input() search: boolean;
  @Input() searchParser = (item: any, search: string) =>
    item.toLowerCase().includes(search.toLowerCase());
  @ViewChild("el") el: ElementRef<any>;
  @ViewChild("searchInput") searchInput: ElementRef<HTMLInputElement>;
  open = false;

  searchValue: string = "";

  zIndex = 1;
  constructor(private _eref: ElementRef) {}

  ngOnInit(): void {}

  change(value: any) {
    this.model = this.modelParser(value);
    this.modelChange.emit(this.model);
    this.optionChange.emit(value);
    this.open = false;
    this.opened.emit(this.open);
  }

  openClose() {
    this.open = !this.open;

    this.opened.emit(this.open);
    if (this.open) {
      this.focusSearch();
      this.el.nativeElement.style.zIndex = 99;
    } else {
      this.blurSearch();
    }
  }

  get value() {
    if (this.model) {
      const option = this.options.find(
        (x) => this.modelParser(x) === this.model
      );
      if (option && option.label) {
        return option.label;
      } else {
        return option;
      }
    }
    return null;
  }

  get shownOptions() {
    if (!this.searchValue || !this.search) {
      return this.options;
    }
    return this.options.filter((item) =>
      this.searchParser(item, this.searchValue)
    );
  }

  @HostListener("document:click", ["$event"])
  handleKeyDown(event: MouseEvent) {
    if (this.open && !this._eref.nativeElement.contains(event.target)) {
      this.open = false;
      this.blurSearch();
      this.opened.emit(this.open);
    }
  }

  private focusSearch() {
    setTimeout(() => {
      if (this.searchInput && this.searchInput.nativeElement) {
        this.searchInput.nativeElement.focus();
        this.searchValue = "";
      }
    });
  }
  private blurSearch() {
    setTimeout(() => {
      if (this.searchInput && this.searchInput.nativeElement) {
        this.searchInput.nativeElement.blur();
        this.searchValue = "";
      }
      this.el.nativeElement.style.zIndex = 1;
    });
  }
}
