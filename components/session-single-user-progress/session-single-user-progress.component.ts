import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { SessionProgressInterface } from "@session-managment/interfaces/session";
import { formatDate } from "@session-managment/utils/partial.utils";

@Component({
  selector: "app-session-single-user-progress",
  templateUrl: "./session-single-user-progress.component.html",
  styleUrls: ["./session-single-user-progress.component.scss"],
})
export class SessionSingleUserProgressComponent implements OnInit {
  @Input() userProgress: SessionProgressInterface[];
  @Input() userId: string;
  @Input() sessionId: string;

  @Output() gotProgresses: EventEmitter<any> = new EventEmitter();

  progresses: any[] = [];

  constructor() {}

  ngOnInit(): void {
    console.log("userProgress", this.userProgress);
  }

  ngAfterViewInit(): void {}

  getDate(data) {
    let formattedDate;
    if (data) {
      formattedDate = formatDate(data, true);
      const fullDate = `${formattedDate} ${new Date(
        data
      ).getHours()}: ${new Date(data).getMinutes()}`;
      return fullDate;
    }
  }
}
