import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SessionSingleUserProgressComponent } from './session-single-user-progress.component';



@NgModule({
  declarations: [SessionSingleUserProgressComponent],
  imports: [CommonModule],
  exports: [SessionSingleUserProgressComponent],
})
export class SessionSingleUserProgressModule {}
