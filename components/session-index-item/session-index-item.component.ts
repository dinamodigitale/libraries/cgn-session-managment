import { Component, Input, OnInit } from '@angular/core';
import { SessionInterface } from '@app/app/interfaces/session';
import { User } from '@session-managment/services/auth.service';
import { CompaniesService } from '@session-managment/services/companies.service';

@Component({
  selector: 'app-session-index-item',
  templateUrl: './session-index-item.component.html',
  styleUrls: ['./session-index-item.component.scss']
})
export class SessionIndexItemComponent implements OnInit {
  @Input() item: SessionInterface
  
  allSessionsUsers: User[] = []
  constructor(private companyService: CompaniesService) { }

  ngOnInit(): void {
    this.companyService.getAllCompanyUsers(this.item.company).subscribe(res => {
      this.allSessionsUsers = res
    })
  }

}
