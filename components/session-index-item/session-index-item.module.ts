import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SessionIndexItemComponent } from './session-index-item.component';
import { UIRouterModule } from '@uirouter/angular';



@NgModule({
  declarations: [SessionIndexItemComponent],
  imports: [
    CommonModule,
    UIRouterModule
  ],
  exports: [SessionIndexItemComponent]
})
export class SessionIndexItemModule { }
