import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { SessionInterface } from '@app/app/interfaces/session';
import { forkJoin } from 'rxjs';

@Component({
  selector: "app-session-header",
  templateUrl: "./session-header.component.html",
  styleUrls: ["./session-header.component.scss"],
})
export class SessionHeaderComponent implements OnInit {
  @Input() item: SessionInterface;
  @Output() onResetAll: EventEmitter<any> = new EventEmitter()
  @Output() archiveSession: EventEmitter<any> = new EventEmitter()
  @Output() startSession: EventEmitter<any> = new EventEmitter()
  @Output() refreshPage: EventEmitter<void> = new EventEmitter()

  constructor() {}

  ngOnInit(): void {}

}
