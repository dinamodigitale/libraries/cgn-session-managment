import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SessionHeaderComponent } from './session-header.component';
import { GroupSelectModule } from '@session-managment/forms/group-select/group-select.module';
import { DirectivesModule } from '@session-managment/directives/directives.module';



@NgModule({
  declarations: [SessionHeaderComponent],
  imports: [
    CommonModule,
    GroupSelectModule,
    DirectivesModule
  ],
  exports: [SessionHeaderComponent]
})
export class SessionHeaderModule { }
