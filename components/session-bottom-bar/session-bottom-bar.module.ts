import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SessionBottomBarComponent } from './session-bottom-bar.component';
import { GroupSelectModule } from '@session-managment/forms/group-select/group-select.module';



@NgModule({
  declarations: [SessionBottomBarComponent],
  imports: [
    CommonModule,
    GroupSelectModule
  ],
  exports: [SessionBottomBarComponent]
})
export class SessionBottomBarModule { }
