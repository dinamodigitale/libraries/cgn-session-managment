import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { SessionInterface } from '@session-managment/interfaces/session';
import { User } from '@session-managment/services/auth.service';
import { UsersService } from '@session-managment/services/users.service';
import { forkJoin } from 'rxjs';

@Component({
  selector: "app-session-bottom-bar",
  templateUrl: "./session-bottom-bar.component.html",
  styleUrls: ["./session-bottom-bar.component.scss"],
})
export class SessionBottomBarComponent implements OnInit {
  @Input() item: SessionInterface;
  @Input() showOptions = false
  @Output() addUser: EventEmitter<User> = new EventEmitter()
  
  availableUsers: User[] = [];
  formattedUsers: User[] = [];

  constructor(private usersService: UsersService) {}

  ngOnInit(): void {
    if (this.item) {
      forkJoin([
        this.usersService.index(),
        this.usersService.usersByCompany(this.item.company),
      ]).subscribe((res) => {
        this.formattedUsers = res[0].data;
        this.availableUsers = res[1].data;
      });
    } else {
      console.log("item.users vuoto");
    }
  }

  itemIdentifier = (item) => item;

  childrenParser(company: User) {
    return company.companies;
  }

  selectSearchParser(company: User, search: string) {
    return (
      company._id.includes(search.toLowerCase()) ||
      company.name.toLowerCase().includes(search.toLowerCase()) ||
      company.surname.toLowerCase().includes(search.toLowerCase()) ||
      company.business.toLowerCase().includes(search.toLowerCase())
    );
  }

  get filteredAvailableUsers() {
    if (this.availableUsers && this.availableUsers.length && this.item.users) {
      return this.availableUsers.filter(
        (x) => !this.item.users.find((y) => y._id === x._id)
      );
    }
    return [];
  }

  get activeOptions() {
    if (this.item.users && this.item.users.length) {
      let users = [];
      this.item.users.forEach((userId) => {
        this.formattedUsers.forEach((user: any) => {
          if (user._id === userId) {
            users.push(user.name || "Provider");
          }
        });
      });
      return users.join(", ");
    }
    return "";
  }
}
