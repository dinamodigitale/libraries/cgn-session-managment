import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AppInterface } from '@session-managment/interfaces/app';
import { SessionInterface } from '@session-managment/interfaces/session';
import { User } from '@session-managment/services/auth.service';

@Component({
  selector: "app-session-progress-filters",
  templateUrl: "./session-progress-filters.component.html",
  styleUrls: ["./session-progress-filters.component.scss"],
})
export class SessionProgressFiltersComponent implements OnInit {
  @Input() defaultSessionApp: AppInterface = null;
  @Input() item: SessionInterface;
  @Input() activeUsersChapters: { [x: string]: any };
  @Output() triggerGridActions: EventEmitter<boolean> = new EventEmitter();
  @Output() changeChapter = new EventEmitter();
  @Output() resetAll = new EventEmitter();
  @Output() selectAll = new EventEmitter();
  @Output() removeSelectedUsers: EventEmitter<void> = new EventEmitter();
  @Output() resetSelectedUsersProgress: EventEmitter<any> = new EventEmitter();

  workOnGrid = false;
  changingChapter = false;
  selectedAllUsers = false;

  choosingChapter = false;

  selectedChapter = {
    identifier: "",
  };

  constructor() {}

  ngOnInit(): void {}

  selectCompanyParser(company: any) {
    if (company) {
      return company.identifier;
    }
    return "";
  }

  selectCompanySearchParser(user: User, search: string) {
    return (
      user._id.includes(search.toLowerCase()) ||
      user.name.toLowerCase().includes(search.toLowerCase())
    );
  }

  onChangeSelect(event) {
    // this.selectedChapter.identifier = event.target.value;
    console.log("form child component", this.selectedChapter.identifier);
    this.changeChapter.emit(this.selectedChapter);
  }

  sendAllSelected() {
    this.selectedAllUsers = !this.selectedAllUsers;
    const deselect = this.selectedAllUsers ? false : true;
    this.selectAll.emit(deselect);
  }
}
