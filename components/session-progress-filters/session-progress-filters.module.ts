import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SessionProgressFiltersComponent } from './session-progress-filters.component';
import { FormsModule } from '@angular/forms';
import { ProSelectModule } from '@session-managment/forms/pro-select/pro-select.module';
import { GroupSelectModule } from '@session-managment/forms/group-select/group-select.module';
import { DirectivesModule } from '@session-managment/directives/directives.module';



@NgModule({
  declarations: [SessionProgressFiltersComponent],
  imports: [
    CommonModule,
    ProSelectModule,
    GroupSelectModule,
    DirectivesModule,
    FormsModule
  ],
  exports: [SessionProgressFiltersComponent]
})
export class SessionProgressFiltersModule { }
