import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import {
  CourseAppChapterInterface,
  CourseInterface,
} from "@app/app/interfaces/course";
import { CompanyInterface } from "@app/app/interfaces/users/company";
import { AppInterface } from "@session-managment/interfaces/app";
import { User } from "@session-managment/services/auth.service";
import { CompaniesService } from "@session-managment/services/companies.service";
import { CoursesService } from "@session-managment/services/courses.service";
import { SessionsService } from "@session-managment/services/sessions.service";
import { UsersService } from "@session-managment/services/users.service";
// import { Store } from "@ngrx/store";
import { StateService } from "@uirouter/core";
import { BehaviorSubject, forkJoin } from "rxjs";
import { concatMap, map, tap } from "rxjs/operators";

@Component({
  selector: "app-create-session-form",
  templateUrl: "./create-session-form.component.html",
  styleUrls: ["./create-session-form.component.scss"],
})
export class CreateSessionFormComponent implements OnInit {
  selectCompanyOptions: CompanyInterface[] = [];
  selectCoursesOptions: CourseInterface[] = [];
  availableUsers: User[] = [];
  avaiablesApps: AppInterface[] = [];

  formattedUsers: User[] = [];

  sessionForm = new FormGroup({
    company: new FormControl(""),
    course: new FormControl(""),
    appId: new FormControl(""),
    chapter: new FormControl(""),
    users: new FormControl([]),
  });

  activeCourse: BehaviorSubject<CourseInterface> = new BehaviorSubject(null);

  selectedApp: AppInterface = null;

  constructor(
    private companiesService: CompaniesService,
    private coursesService: CoursesService,
    private usersService: UsersService,
    private sessionService: SessionsService,
    private stateService: StateService,
  ) {}

  ngOnInit(): void {
    forkJoin([
      this.companiesService.index(),
      this.usersService.index(),
    ]).subscribe((res) => {
      this.selectCompanyOptions = res[0].data;
      this.formattedUsers = res[1].data;
    });
  }

  findItem(id: string, collection: any[]) {
    return collection.find((res) => res._id === id) || null;
  }

  createSessionHandler(values) {
    this.sessionService.create(values).subscribe((res) => {
      // this.store.dispatch(
      //   newLog({
      //     log: {
      //       autoHide: true,
      //       ok: true,
      //       open: true,
      //       type: "success",
      //       message: "Session creata con successo",
      //       error: {
      //         err: "Session creata con successo",
      //       },
      //     },
      //   })
      // );
      setTimeout(() => {
        this.stateService.go("app.private.session-index");
      }, 1000);
    }, err => {
      // this.store.dispatch(
      //   newLog({
      //     log: {
      //       autoHide: false,
      //       ok: false,
      //       open: true,
      //       type: "error",
      //       message: "Errore durante creazione della sessione",
      //       error: {
      //         err: "Errore durante creazione della sessione",
      //       },
      //     },
      //   })
      // );
    });
  }

  selectCompanyParser(company: any) {
    if (company) {
      return company._id;
    }
    return "";
  }

  selectCompanySearchParser(user: User, search: string) {
    return (
      user._id.includes(search.toLowerCase()) ||
      user.name.toLowerCase().includes(search.toLowerCase())
    );
  }

  loadCourses(companyId: string) {
    forkJoin([
      this.coursesService.coursesByCompanyProvider(
        this.findItem(companyId, this.selectCompanyOptions).provider._id
      ),
      this.usersService.usersByCompany(companyId),
    ]).subscribe((res) => {
      this.selectCoursesOptions = res[0];
      this.availableUsers = res[1].data;
    });
  }

  selectCourseParser(course: any) {
    if (course) {
      return course._id;
    }
    return "";
  }

  selectAppParser(app: any) {
    if (app) {
      return app.appId;
    }
    return "";
  }

  selectUserParser(user: any) {
    if (user) {
      return user._id;
    }
    return "";
  }

  childrenParser(company: User) {
    return company.companies;
  }

  selectSearchParser(company: User, search: string) {
    return (
      company._id.includes(search.toLowerCase()) ||
      company.name.toLowerCase().includes(search.toLowerCase()) ||
      company.surname.toLowerCase().includes(search.toLowerCase()) ||
      company.business.toLowerCase().includes(search.toLowerCase())
    );
  }

  getAvailableApps() {
    if (this.sessionForm.value.course) {
      this.coursesService
        .get(this.sessionForm.value.course)
        .pipe(
          map((res) => {
            this.activeCourse.next(res);
            return res.apps.map((app) => app.appId);
          }),
          concatMap((query) => {
            return this.sessionService
              .getAppsByAppId(query)
              .pipe(map((value) => value));
          })
        )
        .subscribe((res: AppInterface[]) => {
          this.avaiablesApps = res;
        });
    }
  }

  setActiveApp(appId) {
    const defaultChap: any = this.activeCourse
      .getValue()
      .apps.find((res: any) => res.appId === appId);

    this.sessionService
      .getAppsByAppId(defaultChap.appId)
      .subscribe((res: AppInterface) => {
        this.selectedApp = res[0];
        this.sessionForm.value.chapter = ""
      });
  }

  setChapterValue(chap) {
    this.selectedApp.chapters.forEach((chap: any) => {
      if (chap.active) {
        chap.active = false;
      }
    });
    chap.active = true;
    this.sessionForm.value.chapter = chap.identifier;
  }

  get activeOptions() {
    if (this.sessionForm.value.users && this.sessionForm.value.users.length) {
      let users = [];
      this.sessionForm.value.users.forEach((userId) => {
        this.formattedUsers.forEach((user) => {
          if (user._id === userId) {
            users.push(user.name || "Provider");
          }
        });
      });
      return users.join(", ");
    }
    return "";
  }

  get disableButton() {
    return (
      !this.sessionForm.value.company ||
      !this.sessionForm.value.course ||
      !this.sessionForm.value.appId ||
      !this.sessionForm.value.chapter
    );
  }
}
