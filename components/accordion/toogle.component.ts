import {
  Component,
  Input,
  Output,
  EventEmitter,
  ChangeDetectionStrategy,
  ContentChildren,
  QueryList,
} from "@angular/core";
import { User } from "../../services/auth.service";
import { SessionIndexProgressComponent } from "../session-index-progress/session-index-progress.component";

@Component({
  selector: "app-toggle",
  template: `
    <div class="toggle">
      <div class="toggle_heading">
        <ng-content select="[accordionHeader]"></ng-content>
      </div>
      <div *ngIf="active" class="toggle_content">
        <ng-content select="[accordionContent]"></ng-content>
      </div>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.Default,
  /* Don't change it with OnPush because it's content projected */
})
export class ToggleComponent {
  @Input() active = false;
  @Input() title = "Default title";
  @Input() user: User;
  @Output() setActive = new EventEmitter();

  @ContentChildren(SessionIndexProgressComponent)
  toggleItem: QueryList<SessionIndexProgressComponent>;

  ngAfterContentInit() {
    // this.toggleItem.map((toggle, i) => {
    //   console.log("inside toogle comp", toggle, i);
    //   toggle.setActive.subscribe(() => this.setActiveItem(i));
    // });
  }

  setActiveItem(index) {
    this.toggleItem.map((toggle, i) => {
      index !== i || toggle.active === true
        ? (toggle.active = false)
        : (toggle.active = true);
    });
    this.setActive.emit(index)
  }
}
