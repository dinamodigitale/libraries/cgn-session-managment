import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccordionComponent } from './accordion.component';
import { ToggleComponent } from './toogle.component';
import { SessionIndexProgressModule } from '../session-index-progress/session-index-progress.module';



@NgModule({
  declarations: [AccordionComponent, ToggleComponent],
  imports: [CommonModule, SessionIndexProgressModule],
  exports: [AccordionComponent, ToggleComponent],
})
export class AccordionModule {}
