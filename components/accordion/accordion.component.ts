import { Component, Input, ContentChildren, QueryList } from "@angular/core";
import { ToggleComponent } from "./toogle.component";

@Component({
  selector: "app-accordion",
  template: `
    <div class="accordion">
      <ng-content></ng-content>
    </div>
  `,
  styleUrls: ["./accordion.scss"],
})
export class AccordionComponent {
  @ContentChildren(ToggleComponent) toggles: QueryList<ToggleComponent>;

  ngAfterContentInit() {
    this.toggles.map((toggle, i) => {
      toggle.setActive.subscribe(() => this.setActive(i));
    });
  }

  setActive(index) {
    this.toggles.map((toggle, i) => {
      index !== i || toggle.active === true
        ? (toggle.active = false)
        : (toggle.active = true);
    });
  }
}
