import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SessionIndexProgressComponent } from './session-index-progress.component';
import { PipesModule } from '@app/app/pipes/pipes.module';
import { FormsModule } from '@angular/forms';
import { DirectivesModule } from '@session-managment/directives/directives.module';
import { ProSelectModule } from '@session-managment/forms/pro-select/pro-select.module';



@NgModule({
  declarations: [SessionIndexProgressComponent],
  imports: [
    CommonModule,
    PipesModule,
    DirectivesModule,
    ProSelectModule,
    FormsModule
  ],
  exports: [SessionIndexProgressComponent]
})
export class SessionIndexProgressModule { }
