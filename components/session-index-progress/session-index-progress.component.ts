import { trigger, transition, style, animate } from "@angular/animations";
import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  Renderer2,
  ViewChild,
  ViewEncapsulation,
} from "@angular/core";
import { AppChapterInterface } from "@session-managment/interfaces/app";
import { SessionInterface, SessionProgressInterface } from "@session-managment/interfaces/session";
import { User } from "@session-managment/services/auth.service";
import { BehaviorSubject, Observable } from "rxjs";
import { filter, map, switchMap, tap } from "rxjs/operators";

@Component({
  selector: "app-session-index-progress",
  templateUrl: "./session-index-progress.component.html",
  styleUrls: ["./session-index-progress.component.scss"],
  // encapsulation: ViewEncapsulation.ShadowDom,
  animations: [
    trigger("growWidth", [
      transition(":enter", [
        style({ width: 0 }),
        animate("500ms", style({ width: "*" })),
      ]),
    ]),
  ],
})
export class SessionIndexProgressComponent implements OnInit, AfterViewInit {
  @Input() item: SessionInterface;
  @Input() chapters: AppChapterInterface[];
  @Input() user: User;
  @Input() showCheckbox = false;
  @Input() selectAll$: BehaviorSubject<boolean>;
  @Input() mappedChapter: { [x: string]: any };
  @Input() firstMessage: SessionProgressInterface;

  @Output() setActive = new EventEmitter();
  @Output() changeChapter = new EventEmitter();
  @Output() addUser = new EventEmitter();
  @Output() removeUser = new EventEmitter();
  @Output() restartUser: EventEmitter<any> = new EventEmitter();

  @ViewChild("progressInnerHtml", { static: false })
  progressInnerHtml: ElementRef;

  active = false;
  hasChapter = false;
  choosingChapter = false;
  singleUserPercentage$: BehaviorSubject<string> = new BehaviorSubject("0%");

  selectedChapter = {
    identifier: "",
  };

  @ViewChild("bar", { static: false })
  bar: ElementRef;

  constructor(private renderer: Renderer2) {}

  ngOnInit(): void {}
  selectCompanyParser(company: any) {
    if (company) {
      return company.identifier;
    }
    return "";
  }
  
  onChangeSelect(event) {
    // this.selectedChapter.identifier = event.target.value.identifier
    this.changeChapter.emit(this.selectedChapter);
    this.selectedChapter.identifier = "" 
  }

  selectCompanySearchParser(user: User, search: string) {
    return (
      user._id.includes(search.toLowerCase()) ||
      user.name.toLowerCase().includes(search.toLowerCase())
    );
  }

  emitRestartChapter() {
    this.restartUser.emit(this.hasChapter ? this.activeUserChap : null);
  }

  ngAfterViewInit() {
    // setTimeout(() => {
    //   this.bar.nativeElement.style.width =
    //     this.singleUserPercentage$.getValue()
    //   this.bar.nativeElement.style.height = "8px";
    // }, 500);
    // setTimeout(() => {
    //   this.renderer.setProperty(
    //     this.progressInnerHtml.nativeElement,
    //     "innerHTML",
    //     `
    //     <style>
    //       .progress::after {
    //          animation: grow 1s forwards !important;
    //       }
    //       @keyframes grow {
    //         0% {
    //           width: 0
    //         }
    //         100% {
    //           width: ${this.singleUserPercentage$.getValue()}
    //         }
    //       }
    //     </style>
    //     `
    //   );
    // }, 500);
    
  }

  get singleUserProgress() {
    return this.singleUserPercentage$.getValue()
  }

  chapterChanged(chap) {
    this.changeChapter.emit(chap);
  }

  checkboxClicked(user: User) {
    user.added = !user.added;
    if (user.added) {
      this.addUser.emit(user);
    } else {
      this.removeUser.emit(user);
    }
  }

  get activeUserChap() {
    if (this.mappedChapter) {
      this.hasChapter = true;
      return this.mappedChapter.chapter;
    } else {
      this.hasChapter = false;
      return "Nessun progress";
    }
  }

  get activeUserChapProgressNumber() {
    if (this.mappedChapter && this.mappedChapter.progress) {
      this.singleUserPercentage$.next(`${this.mappedChapter.progress}%`);
      return this.mappedChapter.progress + "%";
    } else {
      this.singleUserPercentage$.next("0%");
      return 0 + "%";
    }
  }

  get filteredChapters() {
    return this.chapters.filter((res) => {
      if (this.activeUserChap) {
        return res.identifier !== this.activeUserChap;
      } else {
        return true;
      }
    });
  }

  get allUsersSelected$(): Observable<boolean> {
    return this.selectAll$.pipe(map((value) => !!value));
  }
}
