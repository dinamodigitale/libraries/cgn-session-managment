import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QuickCreateSessionFormComponent } from './quick-create-session-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProSelectModule } from '@session-managment/forms/pro-select/pro-select.module';
import { GroupSelectModule } from '@session-managment/forms/group-select/group-select.module';
import { DirectivesModule } from '@session-managment/directives/directives.module';



@NgModule({
  declarations: [QuickCreateSessionFormComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ProSelectModule,
    GroupSelectModule,
    DirectivesModule
  ],
  exports: [QuickCreateSessionFormComponent]
})
export class QuickCreateSessionFormModule { }
