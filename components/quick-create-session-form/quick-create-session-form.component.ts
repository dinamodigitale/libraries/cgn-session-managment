import { Component, Input, OnInit } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { AppInterface } from "@session-managment/interfaces/app";
import { CourseInterface } from "@session-managment/interfaces/course";
import { CompanyInterface } from "@session-managment/interfaces/users/company";
import { User } from "@session-managment/services/auth.service";
import { CompaniesService } from "@session-managment/services/companies.service";
import { CoursesService } from "@session-managment/services/courses.service";
import { SessionsService } from "@session-managment/services/sessions.service";
import { UsersService } from "@session-managment/services/users.service";
import { StateService } from "@uirouter/core";
import { BehaviorSubject, forkJoin } from "rxjs";
import { concatMap, map, tap } from "rxjs/operators";

@Component({
  selector: "app-quick-create-session-form",
  templateUrl: "./quick-create-session-form.component.html",
  styleUrls: ["./quick-create-session-form.component.scss"],
})
export class QuickCreateSessionFormComponent implements OnInit {
  @Input() course;
  @Input() provider;

  // activeCourse: CourseInterface = null;
  selectedCompany: CompanyInterface = null;
  selectCoursesOptions: CourseInterface[] = [];
  availableUsers: User[] = [];
  avaiablesApps: AppInterface[] = [];

  formattedUsers: User[] = [];

  sessionForm = new FormGroup({
    company: new FormControl(""),
    course: new FormControl(""),
    appId: new FormControl(""),
    chapter: new FormControl(""),
    users: new FormControl([]),
  });

  activeCourse: BehaviorSubject<CourseInterface> = new BehaviorSubject(null);
  selectedApp: AppInterface = null;

  constructor(
    private companiesService: CompaniesService,
    private coursesService: CoursesService,
    private usersService: UsersService,
    private sessionService: SessionsService,
    private stateService: StateService
  ) {
    this.coursesService
      .coursesByCompanyProvider(this.provider)
      .pipe(map((value) => value.find((course) => course._id === this.course)))
      .subscribe((res: CourseInterface) => {
        this.activeCourse.next(res);
        this.sessionForm.value.course = this.activeCourse.getValue()?._id;
        this.getAvailableApps();
        this.loadAllUsers();
      });
  }

  ngOnInit(): void {
    setTimeout(() => {
      this.companiesService
        .getCompanyByProvider(this.provider)
        .pipe(
          map((company) => {
            this.selectedCompany = company;
            this.sessionForm.value.company = company._id;
            return company._id;
          }),
          concatMap((id) => {
            return this.usersService
              .usersByCompany(id)
              .pipe(map((users) => users));
          })
        )
        .subscribe((res) => {
          this.availableUsers = res.data;
        });
    });
  }

  loadAllUsers() {
    this.usersService.index().subscribe((res) => {
      this.formattedUsers = res.data;
    });
  }

  getAvailableApps() {
    if (this.activeCourse) {
      this.coursesService
        .get(this.activeCourse.getValue()?._id)
        .pipe(
          map((res) => {
            // this.activeCourse.next(res);
            return res.apps.map((app) => app.appId);
          }),
          concatMap((query) => {
            return this.sessionService
              .getAppsByAppId(query)
              .pipe(map((value) => value));
          })
        )
        .subscribe((res: AppInterface[]) => {
          this.avaiablesApps = res;
        });
    }
  }

  findItem(id: string, collection: any[]) {
    return collection.find((res) => res._id === id) || null;
  }

  createSessionHandler(values) {
    this.sessionService.create(values).subscribe(
      (res) => {
        // this.store.dispatch(
        //   newLog({
        //     log: {
        //       autoHide: true,
        //       ok: true,
        //       open: true,
        //       type: "success",
        //       message: "Session creata con successo",
        //       error: {
        //         err: "Session creata con successo",
        //       },
        //     },
        //   })
        // );
        setTimeout(() => {
          this.stateService.go("app.private.session-index");
        }, 1000);
      },
      (err) => {
        // this.store.dispatch(
        //   newLog({
        //     log: {
        //       autoHide: false,
        //       ok: false,
        //       open: true,
        //       type: "error",
        //       message: "Errore durante creazione della sessione",
        //       error: {
        //         err: "Errore durante creazione della sessione",
        //       },
        //     },
        //   })
        // );
      }
    );
  }

  selectCompanyParser(company: any) {
    if (company) {
      return company._id;
    }
    return "";
  }

  selectCompanySearchParser(user: User, search: string) {
    return (
      user._id.includes(search.toLowerCase()) ||
      user.name.toLowerCase().includes(search.toLowerCase())
    );
  }

  loadCourses(companyId: string) {
    forkJoin([
      this.coursesService.coursesByCompanyProvider(
        this.selectedCompany.provider.toString()
      ),
      this.usersService.usersByCompany(companyId),
    ]).subscribe((res) => {
      this.selectCoursesOptions = res[0];
      this.availableUsers = res[1].data;
    });
  }

  selectCourseParser(course: any) {
    if (course) {
      return course._id;
    }
    return "";
  }

  selectAppParser(app: any) {
    if (app) {
      return app.appId;
    }
    return "";
  }

  selectUserParser(user: any) {
    if (user) {
      return user._id;
    }
    return "";
  }

  childrenParser(company: User) {
    return company.companies;
  }

  selectSearchParser(company: User, search: string) {
    return (
      company._id.includes(search.toLowerCase()) ||
      company.name.toLowerCase().includes(search.toLowerCase()) ||
      company.surname.toLowerCase().includes(search.toLowerCase()) ||
      company.business.toLowerCase().includes(search.toLowerCase())
    );
  }

  // setAppChapter(appId) {
  //   // let defaultChap: CourseAppChapterInterface = this.activeCourse.apps.find(
  //   //   (res) => res.appId === appId
  //   // );
  //   // if (defaultChap && defaultChap.chapterIdentifier) {
  //   //   this.sessionForm.value.chapter = defaultChap.chapterIdentifier;
  //   // }
  // }

  setActiveApp(appId) {
    const defaultChap: any = this.activeCourse
      .getValue()
      .apps.find((res) => res.appId === appId);

    this.sessionService
      .getAppsByAppId(defaultChap.appId)
      .subscribe((res: AppInterface) => {
        this.selectedApp = res[0];
        this.sessionForm.value.chapter = "";
      });
  }

  setChapterValue(chap) {
    this.selectedApp.chapters.forEach((chap) => {
      if (chap.active) {
        chap.active = false;
      }
    });
    chap.active = true;
    this.sessionForm.value.chapter = chap.identifier;
  }

  get activeOptions() {
    if (this.sessionForm.value.users && this.sessionForm.value.users.length) {
      let users = [];
      this.sessionForm.value.users.forEach((userId) => {
        this.formattedUsers.forEach((user) => {
          if (user._id === userId) {
            users.push(user.name || "Provider");
          }
        });
      });
      return users.join(", ");
    }
    return "";
  }

  get disableButton() {
    return (
      !this.sessionForm.value.company ||
      !this.sessionForm.value.course ||
      !this.sessionForm.value.appId ||
      !this.sessionForm.value.chapter
    );
  }
}
