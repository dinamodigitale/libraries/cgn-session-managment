const months: string[] = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
];

const shortMonths: string[] = [
  "01",
  "02",
  "03",
  "04",
  "05",
  "06",
  "07",
  "08",
  "09",
  "10",
  "11",
  "12",
];

function getMonth(month: number) {
  return months[month];
}

function getShorterMonth(month: number) {
  return shortMonths[month];
}

export const formatDate = (date, short?: boolean) => {
  const day = new Date(date).getDate();

  let month;
  if (short) {
    month = getShorterMonth(new Date(date).getMonth());
  } else {
    month = getMonth(new Date(date).getMonth());
  }

  const year = new Date(date).getFullYear();

  const formattedDate = short
    ? `${day}/${month}/${year}`
    : `${day} ${month} ${year}`;
  return formattedDate;
};
