export default class PartialUtils {
  static deepPopulate(payload, resources): any {
    if (!resources || !resources.length) {
      return payload
    }

    Object.keys(payload).forEach(key => {
      if (payload[key] instanceof Object) {
        if (payload[key].thumbnail) {
          payload[key].thumbnail = resources.find(
            res => res._id === payload[key].thumbnail
          )
        } else {
          return PartialUtils.deepPopulate(payload[key], resources)
        }
      }
    })
    return payload
  }

  static generatePortfolio(partials: any[]): any[] {
    const portfolio = []
    partials
      .map(p => p.resources)
      .forEach(p => {
        p.forEach(res => {
          if (res.images && res.images.length) {
            res.images.forEach(img => {
              portfolio.push(img)
            })
          }
        })
      })
    return portfolio
  }
}
