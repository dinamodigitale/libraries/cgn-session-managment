export const mimeTypes = {
  'application/pdf': '.pdf'
}

export const mimeType = (mime: string) => {
  if (mimeTypes[mime]) {
    return mimeTypes[mime].includes('.')
      ? mimeTypes[mime].replace('.', '')
      : mimeTypes[mime]
  }
  return 'file'
}

